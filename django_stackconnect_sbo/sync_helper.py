from django_sap_business_one_models.django_sap_business_one_models.adapters_svcstack import (
    convert_appointment,
)
from django_sap_business_one_models.django_sap_business_one_models.models import *
from django_svcstack_models.django_svcstack_models.models import *


def get_customers(database: str):
    normalized_customers = list()

    all_customers_source = Ocrd.objects.using(database).all()

    for customer in all_customers_source:
        new_customer = MdatCustomers(
            external_id=customer.cardcode,
            name=customer.cardname,
            primary_email=customer.e_mail,
            billing_email=customer.u_dol_einv_email,
            tech_email=customer.u_dol_tech_email,
            enabled=False,
        )

        if customer.u_dol_mdat_enabled == 1:
            new_customer.enabled = True

        normalized_customers.append(new_customer)

    return normalized_customers


def get_appointments(database: str):
    normalized_appointments = list()

    all_appointments_source = Oclg.objects.using(database).all()

    for appointment in all_appointments_source:
        new_appointment = convert_appointment(appointment)

        new_appointment.instance_id = 1
        new_appointment.subject = appointment.details
        new_appointment.customer_temp_id = appointment.cardcode_id
        new_appointment.source_id = 1
        new_appointment.source_extid = appointment.clgcode

        normalized_appointments.append(new_appointment)

    return normalized_appointments


def get_item(database: str, item_code: str = None):
    try:
        item = Oitm.objects.using(database).get(itemcode=item_code)
    except Oitm.DoesNotExist:
        item = None
    return item


def get_items(database: str, item_code_contains: str = None):
    if item_code_contains is not None:
        all_items = Oitm.objects.using(database).filter(
            itemcode__contains=item_code_contains
        )
    else:
        all_items = Oitm.objects.using(database).all()

    return all_items
